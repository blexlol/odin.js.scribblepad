$(function(){
    
  var dim = 16;
    
  makeTable(dim);
   
});


function rgb2hex() {
        var blue = Math.floor((Math.random() * 255) + 1);
        var red = Math.floor((Math.random() * 255) + 1);
        var green = Math.floor((Math.random() * 255) + 1);
        var rgb = blue | (green << 8) | (red << 16);
        return '#' + (0x1000000 + rgb).toString(16).slice(1);
}

function reset(){
   var dim = prompt("Please enter size");
   $('.gridTable').remove();
   makeTable(dim);
}

function makeTable(dim){
    
  var gridCols = dim;
  var gridRows = dim;
  var table = $('<table class="gridTable"></table>');
        
  for(i = 0; i < gridRows; i++){   
    var tr = $('<tr></tr>');
      
    for(k = 0; k < gridCols; k++){
      tr.append("<td>("+i+", "+k+")</td>");
    } 
      
    table.append(tr); 
  }
    
  $('#gridDiv').append(table);
    
  $('td').hover(function(){
      var randomColour = rgb2hex();
      $(this).css('background-color', String(randomColour));  
  }, function(){
      $(this).css('background-color', '#EEEEEE');
  });
    
}